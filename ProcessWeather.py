import json
import sys
import time
import os
import boto3
import requests
import kinesis_producer

class HistdataLink:
    # URL
    ABERPORTH = {
                    "HIST_URL" : "https://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/aberporthdata.txt",
                    "CITY_ID" : 2657789
                }
    
    ARMAGH = {
                    "HIST_URL" : "https://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/armaghdata.txt",
                    "CITY_ID" : 6945982
             }
    
    BRADFORD = {
                    "HIST_URL" : "https://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/bradforddata.txt",
                    "CITY_ID" : 3333131
             }
        
    CARDIFF = {
                    "HIST_URL" : "https://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/cardiffdata.txt",
                    "CITY_ID" : 2653822
             }
        
    HEATHROW = {
                    "HIST_URL" : "https://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/heathrowdata.txt",
                    "CITY_ID" : 7284876
             }
    
    API_KEY = "258a1981cd9f8cf6b879a4d0be4e2551"
    
    CURRENT_WEATHER = "https://api.openweathermap.org/data/2.5/weather?id="
    FORECAST_WEATHER = "https://api.openweathermap.org/data/2.5/forecast?id="

class kinesisprod:
    
    kinesis_client = boto3.client('kinesis', region_name='eu-west-1')
    my_stream_name = 'LoadWeatherData'
    
    def put_to_stream(self,filename):

        with open(filename) as fp:
            for cnt, line in enumerate(fp):
                self.kinesis_client.put_record(StreamName=self.my_stream_name,
                                                Data=line,
                                                PartitionKey=str(cnt))   
  
class loaddata:
    
    s3 = boto3.client('s3')
    kinesis_data = kinesisprod()
    
    
    def __init__(self, user):
        self.user = user
    
    def downloadhist(self,city):
        
        filename = city.split('/')[-1]
        f = open(filename,'a')
        Outfile = requests.get(city, verify=False,stream=False)
        print(Outfile.text,file=f)
        self.s3.upload_file(filename,'loadweatherdatahist',filename)
    
    def downloadcurrentdata(self,city_id):
        current_url = HistdataLink.CURRENT_WEATHER + str(city_id) + "&appid=" + HistdataLink.API_KEY
        
        #print(current_url)
        filename = str(city_id) + "_curr.txt"
        f = open(filename,'a')
        Outfile = requests.get(current_url, verify=False,stream=False)
        print(Outfile.text,file=f)
        self.kinesis_data.put_to_stream(filename)
        
    def downloadforecastdata(self,city_id):
        forecast_url = HistdataLink.FORECAST_WEATHER + str(city_id) + "&appid=" + HistdataLink.API_KEY
        
        #print(current_url)
        filename = str(city_id) + "_fore.txt"
        f = open(filename,'a')
        Outfile = requests.get(forecast_url, verify=False,stream=False)
        print(Outfile.text,file=f)    
        
        self.kinesis_data.put_to_stream(filename)
         
def main():
    Historic_data = loaddata('test')
    
    # ABERPOTH
    Historic_data.downloadhist(HistdataLink.ABERPORTH["HIST_URL"])
    Historic_data.downloadcurrentdata(HistdataLink.ABERPORTH["CITY_ID"])   
    Historic_data.downloadforecastdata(HistdataLink.ABERPORTH["CITY_ID"]) 
          
    # ARMAGH
    Historic_data.downloadhist(HistdataLink.ARMAGH["HIST_URL"])  
    Historic_data.downloadcurrentdata(HistdataLink.ARMAGH["CITY_ID"]) 
    Historic_data.downloadforecastdata(HistdataLink.ARMAGH["CITY_ID"])
    
    # BRADFORD
    Historic_data.downloadhist(HistdataLink.BRADFORD["HIST_URL"]) 
    Historic_data.downloadcurrentdata(HistdataLink.BRADFORD["CITY_ID"])  
    Historic_data.downloadforecastdata(HistdataLink.BRADFORD["CITY_ID"])
    
    # CARDIFF       
    Historic_data.downloadhist(HistdataLink.CARDIFF["HIST_URL"])
    Historic_data.downloadcurrentdata(HistdataLink.CARDIFF["CITY_ID"]) 
    Historic_data.downloadforecastdata(HistdataLink.CARDIFF["CITY_ID"])

    # HEATHROW       
    Historic_data.downloadhist(HistdataLink.HEATHROW["HIST_URL"])
    Historic_data.downloadcurrentdata(HistdataLink.HEATHROW["CITY_ID"]) 
    Historic_data.downloadforecastdata(HistdataLink.HEATHROW["CITY_ID"])
   
    
if __name__ == "__main__":
    main()